---
title: Mumbletreff
header-includes:
    <meta name="keywords" content="Mumble,Community,Talk,Gesellschaft,Computer,IT,Politik,Soziales,Podcast" />
    <meta name="description" content="Ein Sprachchat für alle Themen" />
---

# Head

### Mumbletreff.com
### Ein Sprachchat für alle Themen
### Rede mit uns auf [`voice.mumbletreff.com:10012`](
    mumble://voice.mumbletreff.com:10012?title=Mumbletreff.com) via [Mumble](https://www.mumble.info)

# Content

### Podcast

Wir produzieren einen [Podcast](podcast),
die Episoden sind auf unserer [Podcast-Seite](podcast) zu finden.

### Info

Wir sind eine Community von Menschen, die gerne miteinander sprechen.

Deshalb betreiben wir einen Sprachserver mit der [Mumble-Software](https://www.mumble.info/).

Wir reden gerne über verschiedene Themen, wie z. B.:

- Gesellschaft
- Audio-Technik
- Computer
- Musik
- Medizin
- Politik
- Soziales
- und vieles mehr

Es ist uns dabei sehr wichtig möglichst niemanden auszuschließen und einem
breitem Spektrum an Meinungen einen Platz für den Austausch zu bieten.

Im Gegensatz zu anderen Mumble-Servern liegt unser Fokus **nicht** auf
Computerspielen.

Wir haben auch eine lose Verbindung zum [Chaos Computer Club (CCC)](https://www.ccc.de).
Wenn du etwas über den CCC erfahren möchtest oder schon selber im CCC-Umfeld
tätig bist, dann bist du bei uns genau richtig.

In Zukunft planen wir Podcast-Beiträge in verschiedenen Formaten
(Interview, Themen-Sendung, etc.) zu produzieren.

## So kannst Du bei uns mitmachen

1. Installiere dir die [Mumble-Software](https://www.mumble.info) auf deinen Computer.

2. Finde uns in der offiziellen Serverliste in Deutschland unter dem
Namen "Mumbletreff.com".

    Oder trage direkt die Zugangsdaten in deinen Mumble-Client ein:

        Server: voice.mumbletreff.com
        Port: 10012

    Bei korrekt installiertem Mumble brauchst du nur hier zu klicken:
    [Mumble-Link](mumble://voice.mumbletreff.com:10012?title=Mumbletreff.com&version=1.2.0)

3. Verbinde dich mit dem Server und ein Moderator wird sich in wenigen Minuten
bei dir melden, um dich freizuschalten.

Wir freuen uns über jeden Beitrag!

Wenn die Installation / Einrichtung von Mumble bei dir nicht klappt, kannst du uns
auch auf unserem
Telefon-Gateway\ anrufen:\ [06241\ 857\ 28\ 33](tel:+4962418572833)

Oder schreibe uns eine Twitter-Nachricht:
[Mumbletreff auf Twitter](https://twitter.com/Mumbletreff)

# Foot

[Impressum](impressum.html)
