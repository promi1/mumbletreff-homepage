#!/bin/sh
#TODO: This is very ugly, maybe use a real build system instead? (like meson or cmake)
for i in *.md ; do
    pandoc -s "$i" -o "$(basename "$i" .md).html" --css=style.css --section-divs --standalone
done

for i in podcast/*.md ; do
    pandoc -s "$i" -o "podcast/$(basename "$i" .md).html" --css=../style.css --section-divs --standalone
done

for i in podcast/episodes/5/*.md ; do
    pandoc -s "$i" -o "podcast/episodes/5/$(basename "$i" .md).html" --css=../../../style.css --section-divs --standalone
done
