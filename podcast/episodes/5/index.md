---
title: Mumbletreff Podcast Episode 5
header-includes:
    <meta name="keywords" content="Mumble,Community,Talk,Gesellschaft,Computer,IT,Politik,Soziales,Podcast" />
    <meta name="description" content="Ein Sprachchat für alle Themen" />
---

# Head

### [Mumbletreff.com](../../..)
### Ein Sprachchat für alle Themen
### Rede mit uns auf [`voice.mumbletreff.com:10012`](
    mumble://voice.mumbletreff.com:10012?title=Mumbletreff.com) via [Mumble](https://www.mumble.info)

# Content

### [Podcast](../..) Episode 5

### Umweltschutz und Nachhaltigkeit

- [Episode im MP3-Format](mumbletreff_podcast_005.mp3)
- [Episode im Opus-Format](mumbletreff_podcast_005.opus)

# Foot

[Impressum](../../../impressum.html)
