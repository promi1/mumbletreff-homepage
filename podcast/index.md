---
title: Mumbletreff Podcast
header-includes:
    <meta name="keywords" content="Mumble,Community,Talk,Gesellschaft,Computer,IT,Politik,Soziales,Podcast" />
    <meta name="description" content="Ein Sprachchat für alle Themen" />
---

# Head

### [Mumbletreff.com](..)
### Ein Sprachchat für alle Themen
### Rede mit uns auf [`voice.mumbletreff.com:10012`](
    mumble://voice.mumbletreff.com:10012?title=Mumbletreff.com) via [Mumble](https://www.mumble.info)

# Content

### Podcast

- Episode 5 - Umweltschutz und Nachhaltigkeit [mp3](
    episodes/5/mumbletreff_podcast_005.mp3) [opus](
        episodes/5/mumbletreff_podcast_005.opus)
- Episode 4 - Linux-Desktopumgebungen [mp3](
    episodes/4/mumbletreff_podcast_004.mp3) [opus](
        episodes/4/mumbletreff_podcast_004.opus)
- Episode 3 - Das Linux Terminal [mp3](
    episodes/3/mumbletreff_podcast_003.mp3) [opus](
        episodes/3/mumbletreff_podcast_003.opus)
- Episode 2 - Vorstellung des Projekts [mp3](
    episodes/2/mumbletreff_podcast_002.mp3) [opus](
        episodes/2/mumbletreff_podcast_002.opus)
- Episode 1 - Impfpflicht, Landtagswahlen, SPD-Vorsitz, etc. [mp3](
    episodes/1/mumbletreff_podcast_001.mp3) [opus](
        episodes/1/mumbletreff_podcast_001.opus)

[Sendeplan / Kalender](https://calendar.google.com/calendar/embed?src=0efmbevt20c55ehmse0neb6gvg@group.calendar.google.com&ctz=Europe/Zurich)

# Foot

[Impressum](../impressum.html)
